public enum MoveType {
    COOPERATE("co"),
    CHEAT("ch");
    String value;
    private MoveType(String value) {
        this.value = value;
    }
}

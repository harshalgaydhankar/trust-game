import java.util.Scanner;

public class Player {

    private Scanner input;
    private int score;

    public Player(Scanner sc)
    {
        this.input = sc;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String makeMove() {

        int userInput = input.nextInt();
        if(userInput == 1)
            return MoveType.COOPERATE.value;
        else if(userInput == 2)
            return  MoveType.CHEAT.value;
        return null;
    }
}


public class Score {

    public Score(int player1, int player2) {
        this.player1 = player1;
        this.player2 = player2;
    }
    private int player1;
    private int player2;

    public int getPlayer1() {
        return player1;
    }

    public int getPlayer2() {
        return player2;
    }

}

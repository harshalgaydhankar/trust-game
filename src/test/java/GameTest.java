import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class GameTest {

    @Test
    public void shouldReturnScoreAfterFirstRound() {
        Player player1 = Mockito.mock(Player.class);
        Player player2 = Mockito.mock(Player.class);
        when(player1.makeMove()).thenReturn(MoveType.COOPERATE.value);
        when(player2.makeMove()).thenReturn(MoveType.COOPERATE.value);


        Machine machine = Mockito.mock(Machine.class);
        when(machine.compute(MoveType.COOPERATE.value, MoveType.COOPERATE.value)).thenReturn(new Score(2, 2));

        PrintStream out = Mockito.mock(PrintStream.class);
        System.setOut(out);

        Game game = new Game(player1, player2, machine);

        int rounds = 2;
        game.play(rounds);

        verify(player1, Mockito.times(rounds)).makeMove();
        verify(player2, Mockito.times(rounds)).makeMove();

        verify(machine, Mockito.times(rounds)).compute(MoveType.COOPERATE.value, MoveType.COOPERATE.value);

        verify(out, Mockito.times(rounds)).println("Player1 : 2 , Player2 : 2");


    }

}

import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class GameTestIT {

    @Test
    public void shouldPlayMultipleRounds(){
        Scanner sc = new Scanner("1 \n 2");
        Scanner sc1 = new Scanner("1 \n 1");
        Player player1 = new Player(sc);
        Player player2 = new Player(sc1);

        Machine machine = new Machine();

        Game game = new Game(player1, player2, machine);

        game.play(2);

        assertEquals(5,player1.getScore());
        assertEquals(1,player2.getScore());
    }
}

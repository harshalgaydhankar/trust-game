import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class PlayerTest {

    @Test
    public void shouldReturnCooperateMoveforPlayer(){
        Player player = new Player(new Scanner("1"));
        String moveType = player.makeMove();
        assertEquals(MoveType.COOPERATE.value, moveType);
    }

    @Test
    public void shouldReturnCheatMoveforPlayer(){

        Player player = new Player(new Scanner("2"));
        String moveType = player.makeMove();
        assertEquals(MoveType.CHEAT.value, moveType);
    }

    @Test
    public void shouldReturnBadMoveforInvalid(){

        Player player = new Player(new Scanner("5"));
        String moveType = player.makeMove();
        assertEquals( null, moveType);
    }
}

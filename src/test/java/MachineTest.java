import org.junit.Assert;
import org.junit.Test;

public class MachineTest {

    Machine machine = new Machine();

    @Test
    public void testComputeForCoCo() {
        Score score = machine.compute(MoveType.COOPERATE.value, MoveType.COOPERATE.value);
        Assert.assertEquals(2, score.getPlayer1());
        Assert.assertEquals(2, score.getPlayer2());
    }

    @Test
    public void testComputeForCoCh() {
        Score score = machine.compute(MoveType.COOPERATE.value, MoveType.CHEAT.value);
        Assert.assertEquals(-1, score.getPlayer1());
        Assert.assertEquals(3, score.getPlayer2());
    }

    @Test
    public void testComputeForChCo() {
        Score score = machine.compute(MoveType.CHEAT.value, MoveType.COOPERATE.value);
        Assert.assertEquals(3, score.getPlayer1());
        Assert.assertEquals(-1, score.getPlayer2());
    }

    @Test
    public void testComputeForChCh() {
        Score score = machine.compute(MoveType.CHEAT.value, MoveType.CHEAT.value);
        Assert.assertEquals(0, score.getPlayer1());
        Assert.assertEquals(0, score.getPlayer2());
    }

}
